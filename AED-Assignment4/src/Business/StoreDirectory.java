/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankur
 */
public class StoreDirectory {
    
    private ArrayList<Store> StoreList;
    private Store store;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
    
    public StoreDirectory(){
     StoreList = new ArrayList<Store>();
     }
    
     public ArrayList<Store> getStoreList() {
        return StoreList;
    }
   public Store addStore(){
    Store s = new Store();
    StoreList.add(s);
    return s;
    }
    
    public void  removeStore(Store s){
      StoreList.remove(s);
      }
}
