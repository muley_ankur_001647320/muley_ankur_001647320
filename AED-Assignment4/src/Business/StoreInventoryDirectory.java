/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankur
 */
public class StoreInventoryDirectory {
    
    private ArrayList<StoreInventory> inventoryList;
    private StoreInventory storeInventory;

    public StoreInventory getStoreInventory() {
        return storeInventory;
    }

    public void setStoreInventory(StoreInventory storeInventory) {
        this.storeInventory = storeInventory;
    }

    public ArrayList<StoreInventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(ArrayList<StoreInventory> inventoryList) {
        this.inventoryList = inventoryList;
    }
    
    public StoreInventoryDirectory(){
     inventoryList = new ArrayList<StoreInventory>();
     }
     public StoreInventory addStoreInventory(){
    StoreInventory si = new StoreInventory();
    inventoryList.add(si);
    return si;
    }
    
    public void  deleteStoreInventory(StoreInventory si){
      inventoryList.remove(si);
      }
}
