/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankur
 */
public class Store {
    
    private static int count;
    private String storeName;
    private String storeLocality;
    private int storeID;
   private StoreInventoryDirectory storeInventoryDirectory;

    public StoreInventoryDirectory getStoreInventoryDirectory() {
        return storeInventoryDirectory;
    }

    public void setStoreInventoryDirectory(StoreInventoryDirectory storeInventoryDirectory) {
        this.storeInventoryDirectory = storeInventoryDirectory;
    }
   
    
 
    
    public Store(){
        count++;
        storeID = count; 
        
    }
    
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreLocality() {
        return storeLocality;
    }

    public void setStoreLocality(String storeLocality) {
        this.storeLocality = storeLocality;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Store.count = count;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }
    
    
}
