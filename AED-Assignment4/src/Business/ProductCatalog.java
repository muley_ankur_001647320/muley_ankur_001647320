/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 */
public class ProductCatalog {
    
 private ArrayList<Product> productList; 
 private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
 
 public ProductCatalog(){
     productList = new ArrayList<Product>();
 }

    public ArrayList<Product> getProductList() {
        return productList;
    }
 
    public Product addProduct(){
    Product p = new Product();
    productList.add(p);
    return p;
    }
    
    public void  deleteProduct(Product p){
      productList.remove(p);
      }
    
//    public Product searchProduct(){
//        
//    }
//    
    
}
