/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankur
 */
public class VitalSign {

    private int RespiratoryRate;
    private int HeartRate;
    private int SystolicBloodPressure;
    private double WeightinPounds;
    private String DateofVitalSign;

    public int getRespiratoryRate() {
        return RespiratoryRate;
    }

    public void setRespiratoryRate(int RespiratoryRate) {
        this.RespiratoryRate = RespiratoryRate;
    }

    public int getHeartRate() {
        return HeartRate;
    }

    public void setHeartRate(int HeartRate) {
        this.HeartRate = HeartRate;
    }

    public double getSystolicBloodPressure() {
        return SystolicBloodPressure;
    }

    public void setSystolicBloodPressure(int SystolicBloodPressure) {
        this.SystolicBloodPressure = SystolicBloodPressure;
    }

    public double getWeightinPounds() {
        return WeightinPounds;
    }

    public void setWeightinPounds(double WeightinPounds) {
        this.WeightinPounds = WeightinPounds;
    }

    public String getDateofVitalSign() {
        return DateofVitalSign;
    }

    public void setDateofVitalSign(String DateofVitalSign) {
        this.DateofVitalSign = DateofVitalSign;
    }

    @Override
    public String toString(){
      return this.DateofVitalSign;
    }
}
