/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankur
 */
public class Patient {
    private String PatientName;
    private String PatientID;
    private String Age;
    private String PrimaryDoctorName;
    private String PreferredPharmacy;

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String PatientName) {
        this.PatientName = PatientName;
    }

    public String getPatientID() {
        return PatientID;
    }

    public void setPatientID(String PatientID) {
        this.PatientID = PatientID;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }

    public String getPrimaryDoctorName() {
        return PrimaryDoctorName;
    }

    public void setPrimaryDoctorName(String PrimaryDoctorName) {
        this.PrimaryDoctorName = PrimaryDoctorName;
    }

    public String getPreferredPharmacy() {
        return PreferredPharmacy;
    }

    public void setPreferredPharmacy(String PreferredPharmacy) {
        this.PreferredPharmacy = PreferredPharmacy;
    }
            
    
}
