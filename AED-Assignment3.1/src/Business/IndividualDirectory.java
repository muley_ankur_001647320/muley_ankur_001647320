/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankur
 */
public class IndividualDirectory {
    
    private ArrayList<Individual> individualDirectory;
    private Individual individual;

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }


public IndividualDirectory()
{
 individualDirectory = new ArrayList <>();
}

    public ArrayList<Individual> getindividualDirectory() {
        return individualDirectory;
    }

    public void setIndividualDirectory(ArrayList<Individual> individualDirectory) {
        this.individualDirectory = individualDirectory;
    }

    public Individual addIndividual(){
      Individual i = new Individual() ;
      individualDirectory.add(i);
      return i;
    }
    
    public void deleteIndividual(Individual i){
        individualDirectory.remove(i);
    }
  
    public Individual searchIndividual(String SSN){
        for(Individual i : individualDirectory){
            if (i.getSSN().equals(SSN)){
                return i;
            }
        }
                
        return null;        
    }

//    public int getSelectedRow() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }







