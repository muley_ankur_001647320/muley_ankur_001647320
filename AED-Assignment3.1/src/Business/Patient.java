/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
/**
 *
 * @author Ankur
 */
public class Patient {
   
    private String PrimaryDoctorName;
    private String PreferredPharmacy;
    private VitalSignHistory vitalSignHistory;

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
  
     public String getPrimaryDoctorName() {
        return PrimaryDoctorName;
    }

    public void setPrimaryDoctorName(String PrimaryDoctorName) {
        this.PrimaryDoctorName = PrimaryDoctorName;
    }

    public String getPreferredPharmacy() {
        return PreferredPharmacy;
    }

    public void setPreferredPharmacy(String PreferredPharmacy) {
        this.PreferredPharmacy = PreferredPharmacy;
    }

    
}
