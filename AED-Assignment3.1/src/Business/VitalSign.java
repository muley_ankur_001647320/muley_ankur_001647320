/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankur
 */
public class VitalSign {
    private String DateOfVitalSign;
    private int RespiratoryRate;
    private int HeartRate;
    private int SystolicBloodPressure;
    private double WeightinPounds;

    public String getDateOfVitalSign() {
        return DateOfVitalSign;
    }

    public void setDateOfVitalSign(String DateOfVitalSign) {
        this.DateOfVitalSign = DateOfVitalSign;
    }


    public int getRespiratoryRate() {
        return RespiratoryRate;
    }

    public void setRespiratoryRate(int RespiratoryRate) {
        this.RespiratoryRate = RespiratoryRate;
    }

    public int getHeartRate() {
        return HeartRate;
    }

    public void setHeartRate(int HeartRate) {
        this.HeartRate = HeartRate;
    }

    public int getSystolicBloodPressure() {
        return SystolicBloodPressure;
    }

    public void setSystolicBloodPressure(int SystolicBloodPressure) {
        this.SystolicBloodPressure = SystolicBloodPressure;
    }

    public double getWeightinPounds() {
        return WeightinPounds;
    }

    public void setWeightinPounds(double WeightinPounds) {
        this.WeightinPounds = WeightinPounds;
            }
    
    public String toString() {
        return this.DateOfVitalSign;
    }

 
    
}
