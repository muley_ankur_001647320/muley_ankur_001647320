/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Individual;
import Business.IndividualDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ankur
 */
public class PatientManagerWorkAreaJPanel extends javax.swing.JPanel {
    private IndividualDirectory individualDirectory;
    private JPanel userProcessContainer;

    /**
     * Creates new form PatientWorkAreaJPanel
     */
    public PatientManagerWorkAreaJPanel(IndividualDirectory id,JPanel upc) {
        initComponents();
        this.userProcessContainer =upc;
        this.individualDirectory =id;    
        refreshTable();
    }
    
    public void refreshTable(){
        DefaultTableModel dtm = (DefaultTableModel) tblViewPatient.getModel();
        dtm.setRowCount(0);
    
        for(Individual i: individualDirectory.getindividualDirectory()){
            if(i.getIsPatient().equals("true")){
            Object[] row=new Object[6];
            row[0] = i;
            row[1]= i.getName();
            row[2]= i.getAge();
            row[3]= i.getSex();
            row[4]= i.getPatient().getPrimaryDoctorName();          
           row[5]= i.getPatient().getPreferredPharmacy();
            dtm.addRow(row);
        }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bttnCreateVitalSign = new javax.swing.JButton();
        bttnViewPatient = new javax.swing.JButton();
        bttnBack = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        bttnSearch = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblViewPatient = new javax.swing.JTable();
        bttnRefreshTable = new javax.swing.JButton();
        bttnViewVitalSign = new javax.swing.JButton();

        bttnCreateVitalSign.setText("Create Vital Sign");
        bttnCreateVitalSign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnCreateVitalSignActionPerformed(evt);
            }
        });

        bttnViewPatient.setText("View Patient Details");
        bttnViewPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnViewPatientActionPerformed(evt);
            }
        });

        bttnBack.setText("<<  Back");
        bttnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnBackActionPerformed(evt);
            }
        });

        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });

        bttnSearch.setText("Search By SSN");
        bttnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSearchActionPerformed(evt);
            }
        });

        tblViewPatient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SSN", "Patient Name", "Patient Age", "Patient Sex", "PrimaryDoctorName", "Preferred Pharmacy"
            }
        ));
        jScrollPane2.setViewportView(tblViewPatient);

        bttnRefreshTable.setText("Refresh Table");
        bttnRefreshTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnRefreshTableActionPerformed(evt);
            }
        });

        bttnViewVitalSign.setText("View Vital Sign ");
        bttnViewVitalSign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnViewVitalSignActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(bttnCreateVitalSign, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bttnViewPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(bttnRefreshTable, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bttnViewVitalSign, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bttnBack)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(62, 62, 62)
                                .addComponent(bttnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(675, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(bttnRefreshTable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bttnCreateVitalSign)
                            .addComponent(bttnViewVitalSign))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bttnViewPatient)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bttnSearch)))
                .addGap(145, 145, 145)
                .addComponent(bttnBack)
                .addContainerGap(335, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bttnCreateVitalSignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnCreateVitalSignActionPerformed
        // TODO add your handling code here:
     int selectedRow = tblViewPatient.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select a patient from the table first to Create VitalSign", "Warning", JOptionPane.WARNING_MESSAGE);
        } 
      else{
          Individual individual= (Individual)tblViewPatient.getValueAt(selectedRow, 0);
          CreateVitalSignJPanel createVitalSignJpanel= new CreateVitalSignJPanel(individual,userProcessContainer);
          userProcessContainer.add("CreateVitalSignJPanel",createVitalSignJpanel);
          CardLayout layout = (CardLayout) userProcessContainer.getLayout();
          layout.next(userProcessContainer);
      }
        
    }//GEN-LAST:event_bttnCreateVitalSignActionPerformed

    private void bttnViewPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnViewPatientActionPerformed
        // TODO add your handling code here:
     int selectedRow = tblViewPatient.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select a patient from the table first to View Patient Details", "Warning", JOptionPane.WARNING_MESSAGE);
        } 
      else
        {
        
        Individual individual= (Individual)tblViewPatient.getValueAt(selectedRow, 0);
        ViewPatientJPanel viewPatientJPanel= new ViewPatientJPanel(individual,userProcessContainer);
        userProcessContainer.add("ViewPatientJPanel",viewPatientJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
         }      
    }//GEN-LAST:event_bttnViewPatientActionPerformed

   
    private void bttnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout .previous(userProcessContainer);
    }//GEN-LAST:event_bttnBackActionPerformed

    private void bttnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSearchActionPerformed
        // TODO add your handling code here:
        String key = txtSearch.getText();
        if(key==null||key.length() ==0){
            JOptionPane.showMessageDialog(null, "Please provide SSN to search", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        else{
          Individual result = individualDirectory.searchIndividual(key);
          
          if(result!=null){
          SearchPatientJPanel panel = new SearchPatientJPanel(result, userProcessContainer);
          userProcessContainer.add("SearchPatientJPanel",panel);
          CardLayout layout = (CardLayout) userProcessContainer.getLayout();
          layout.next(userProcessContainer);
          }else{
           JOptionPane.showMessageDialog(null, "Patient to search dosen't exist in the records.", "Information", JOptionPane.INFORMATION_MESSAGE);
          }
        }
    }//GEN-LAST:event_bttnSearchActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void bttnRefreshTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnRefreshTableActionPerformed
        // TODO add your handling code here:
        refreshTable();
    }//GEN-LAST:event_bttnRefreshTableActionPerformed

    private void bttnViewVitalSignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnViewVitalSignActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblViewPatient.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select a patient from the table first to view VitalSign", "Warning", JOptionPane.WARNING_MESSAGE);
        } 
      else{
          Individual individual= (Individual)tblViewPatient.getValueAt(selectedRow, 0);
          ViewJPanel viewJPanel= new ViewJPanel(individual,userProcessContainer);
          userProcessContainer.add("ViewJPanel",viewJPanel);
          CardLayout layout = (CardLayout) userProcessContainer.getLayout();
          layout.next(userProcessContainer);
      }
    }//GEN-LAST:event_bttnViewVitalSignActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bttnBack;
    private javax.swing.JButton bttnCreateVitalSign;
    private javax.swing.JButton bttnRefreshTable;
    private javax.swing.JButton bttnSearch;
    private javax.swing.JButton bttnViewPatient;
    private javax.swing.JButton bttnViewVitalSign;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblViewPatient;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
