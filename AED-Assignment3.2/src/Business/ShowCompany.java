/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankur
 */
public class ShowCompany {
    

private void newSupplier() {
        String[] sName = new String[]{"Dell", "HP", "Toshiba", "Apple", "Lenovo"};
        SupplierDirectory supplierDirectory = new SupplierDirectory();
        for (int i = 0; i < sName.length; i++) {
            Supplier s = supplierDirectory.addSupplier();
            s.setSupplierName(sName[i]);
            System.out.println(sName[i] + "sucesfully added. ");
            insertProductCatalog(s, i);
        }
    }

    private void insertProductCatalog(Supplier s, int i) {
        ProductCatalog productCalatog = new ProductCatalog();
        switch (i) {
            case 0:
                String[] productdell = new String[]{"Dell1", "Dell2", "Dell3", "Dell4", "Dell5", "Dell6", "Dell7", "Dell8", "Dell9", "Dell10"};

                for (int j = 0; j < productdell.length; j++) {
                    Product p = productCalatog.addProduct();
                    p.setProductName(productdell[j]);
                    System.out.println(s.getSupplierName() + "-" + productdell[j]);
                }
                break;

            case 1:
                String[] producthp = new String[]{"HP1", "HP2", "HP3", "HP4", "HP5", "HP6", "HP7", "HP8", "HP9", "HP10"};

                for (int j = 0; j < producthp.length; j++) {
                    Product p = productCalatog.addProduct();
                    p.setProductName(producthp[j]);
                    System.out.println(s.getSupplierName() +  "-"  + producthp[j]);
                }
                break;

            case 2:
                String[] producttoshiba = new String[]{"Toshiba1", "Toshiba2", "Toshiba3", "Toshiba4", "Toshiba5", "Toshiba6", "Toshiba7", "Toshiba8", "Toshiba9", "Toshiba10"};

                for (int j = 0; j < producttoshiba.length; j++) {
                    Product p = productCalatog.addProduct();
                    p.setProductName(producttoshiba[j]);
                    System.out.println(s.getSupplierName() +  "-"  + producttoshiba[j]);
                }
                break;

            case 3:
                String[] productapple = new String[]{"Apple1", "Apple2", "Apple3", "Apple4", "Apple5", "Apple6", "Apple7", "Apple8", "Apple9", "Apple10"};

                for (int j = 0; j < productapple.length; j++) {
                    Product p = productCalatog.addProduct();
                    p.setProductName(productapple[j]);
                    System.out.println(s.getSupplierName() +  "-"  + productapple[j]);
                }
                break;

            case 4:
                String[] productlenovo = new String[]{"Lenovo1", "Lenovo2", "Lenovo3", "Lenovo4", "Lenovo5", "Lenovo6", "Lenovo7", "Lenovo8", "Lenovo9", "Lenovo10"};

                for (int j = 0; j < productlenovo.length; j++) {
                    Product p = productCalatog.addProduct();
                    p.setProductName(productlenovo[j]);
                    System.out.println(s.getSupplierName() +  "-"  + productlenovo[j]);
                }
                break;
        }

    }

    public static void main(String[] args) {

        ShowCompany displayCompany = new ShowCompany();
        displayCompany.newSupplier();

    }

}	